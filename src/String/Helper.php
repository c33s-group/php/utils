<?php

declare(strict_types=1);

namespace C33s\Utils\String;

class Helper
{
    /**
     * Fake check if the current String could be a Regex.
     *
     * https://stackoverflow.com/questions/172303/is-there-a-regular-expression-to-detect-a-valid-regular-expression
     * https://stackoverflow.com/questions/10778318/test-if-a-string-is-regex
     */
    public static function isRegex(string $string, string $regexCharacter = '/'): bool
    {
        return $regexCharacter === $string[0] && $regexCharacter === $string[-1];
    }

    /**
     * Checks if a string starts with a specific string.
     *
     * @deprecated since 1.1, use php8 or symfony/polyfill-php80 instead.
     *
     * https://stackoverflow.com/questions/834303/startswith-and-endswith-functions-in-php
     */
    public static function startsWith(string $haystack, string $needle): bool
    {
        trigger_deprecation(
            'c33s/utils',
            '1.1',
            'The method "%s" is deprecated, use "%s" of php8 (symfony/polyfill-php80) instead.',
            'startsWith',
            'str_starts_with'
        );

        return 0 === strpos($haystack, $needle);
    }

    /**
     * Checks if a string ends with a specific string.
     *
     * @deprecated since 1.1, use php8 or symfony/polyfill-php80 instead.
     *
     * https://stackoverflow.com/questions/834303/startswith-and-endswith-functions-in-php
     */
    public static function endsWith(string $haystack, string $needle): bool
    {
        trigger_deprecation(
            'c33s/utils',
            '1.1',
            'The method "%s" is deprecated, use "%s" of php8 (symfony/polyfill-php80) instead.',
            'endsWith',
            'str_ends_with'
        );
        $length = strlen($needle);

        return 0 === $length ||
            (substr($haystack, -$length) === $needle);
    }

    /**
     * Checks if a string contains another string.
     *
     * @deprecated since 1.1, use php8 or symfony/polyfill-php80 instead.
     */
    public static function contains(string $haystack, string $needle): bool
    {
        trigger_deprecation(
            'c33s/utils',
            '1.1',
            'The method "%s" is deprecated, use "%s" of php8 (symfony/polyfill-php80) instead.',
            'contains',
            'str_contains'
        );

        return false !== strpos($haystack, $needle);
    }
}
