<?php

declare(strict_types=1);

namespace C33s\Utils\String;

use InvalidArgumentException;
use Stringy\Stringy;

/**
 * Wrapper for `danielstjules/stringy` to simulate parts of `symfony/string`.
 *
 * @see https://packagist.org/packages/danielstjules/stringy
 * @see https://symfony.com/doc/current/components/string.html
 */
class UnicodeString implements UnicodeStringInterface
{
    /**
     * @var Stringy
     */
    protected $wrappedObject;

    public function __construct(string $string = '')
    {
        $normalizedString = normalizer_is_normalized($string) ? $string : normalizer_normalize($string);
        if (false === $normalizedString) {
            throw new InvalidArgumentException('Invalid UTF-8 string.');
        }
        $this->wrappedObject = Stringy::create($normalizedString);
    }

    public function truncate(int $length, string $ellipsis = '', bool $cut = true): self
    {
        if (true === $cut) {
            $this->wrappedObject = $this->wrappedObject->truncate($length, $ellipsis);
        } else {
            $this->wrappedObject = $this->wrappedObject->safeTruncate($length, $ellipsis);
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->wrappedObject->__toString();
    }
}
