<?php

declare(strict_types=1);

namespace C33s\Utils\String;

/**
 * @param string $string
 */
function u($string = ''): UnicodeString
{
    return new UnicodeString($string ?? '');
}

//function b(?string $string = ''): ByteString
//{
//    return new ByteString($string ?? '');
//}
//
///**
// * @return UnicodeString|ByteString
// */
//function s(?string $string = ''): AbstractString
//{
//    $string = $string ?? '';
//
//    return preg_match('//u', $string) ? new UnicodeString($string) : new ByteString($string);
//}
