<?php

declare(strict_types=1);

namespace C33s\Utils;

use Phar;

class PharHelper
{
    public static function isPhar(): bool
    {
        return '' !== Phar::running();
    }

    public static function directory(): string
    {
        return dirname(Phar::running(false));
    }
}
