<?php

declare(strict_types=1);

namespace C33s\Utils;

class ProxyTools
{
    /**
     * Proxyable Schemes.
     */
    private const SCHEME_ENVIRONMENT_VAR_MAP = [
        'http' => 'HTTP_PROXY',
        'https' => 'HTTPS_PROXY',
    ];

    private const PROTOCOLS = [
        'http://',
        'https://',
        'ftp://',
        'ftps://',
        'sftp://',
        'socks://',
    ];

    /**
     * @param resource $context
     */
    public static function proxyAwareFileGetContents(string $path, $context = null): ?string
    {
        if (self::hasToUseProxy($path)) {
            //https://github.com/phpstan/phpstan/issues/3287
            /** @var string[] */
            $urlInfo = parse_url($path);
            $scheme = $urlInfo['scheme'] ?? 'https';
            $proxy = self::fetchProxy($scheme);
            $context = self::addProxyToStreamContext($proxy, $context);
        }

        return self::fileGetContents($path, $context);
    }

    /**
     * @param resource $context
     */
    private static function fileGetContents(string $path, $context): ?string
    {
        $result = file_get_contents($path, false, $context);

        if (false === $result) {
            $result = null;
        }

        return $result;
    }

    public static function hasToUseProxy(string $path): bool
    {
        if (stream_is_local($path)) {
            return false;
        }

        if (!self::isUrl($path)) {
            return false;
        }

        //https://github.com/phpstan/phpstan/issues/3287
        /** @var string[] */
        $urlInfo = parse_url($path);
        if (!self::environmentProxyVarFound($urlInfo['scheme'] ?? 'https')) {
            return false;
        }

        if (self::hasNoProxy() && self::isExcluded($urlInfo['host'] ?? '')) {
            return false;
        }

        return true;
    }

    /**
     * Gets the current set https_proxy. if a path is provided, no proxy is also checked.
     *
     * @param mixed|null $path
     * @param mixed      $stripProtocol
     * @param mixed      $defaultScheme
     */
    public static function getProxy($path = null, $stripProtocol = true, $defaultScheme = 'https'): ?string
    {
        if (null !== $path && self::hasToUseProxy($path)) {
            //https://github.com/phpstan/phpstan/issues/3287
            /** @var string[] */
            $urlInfo = parse_url($path);
            $scheme = $urlInfo['scheme'] ?? $defaultScheme;

            return self::fetchProxy($scheme);
        }

        $proxy = self::fetchProxy($defaultScheme);
        if (true === $stripProtocol) {
            $proxy = self::removeProtocol($proxy);
        }

        if ('' === $proxy) {
            return null;
        }

        return $proxy;
    }

    private static function isUrl(string $url): bool
    {
        return !(false === filter_var($url, FILTER_VALIDATE_URL));
    }

    private static function environmentProxyVarFound(string $scheme): bool
    {
        if (!array_key_exists($scheme, self::SCHEME_ENVIRONMENT_VAR_MAP)) {
            return false;
        }
        $proxy = getenv(self::SCHEME_ENVIRONMENT_VAR_MAP[$scheme]);
        if (empty($proxy)) {
            return false;
        }

        return true;
    }

    private static function isExcluded(string $host): bool
    {
        $noProxy = getenv('NO_PROXY');
        if (false === $noProxy) {
            $noProxy = '';
        }

        if (str_contains($noProxy, $host)) {
            return true;
        }

        return false;
    }

    private static function hasNoProxy(): bool
    {
        $noProxy = getenv('NO_PROXY');
        if (empty($noProxy)) {
            return false;
        }
        if (!is_string($noProxy)) {
            return false;
        }

        return true;
    }

    private static function fetchProxy(string $scheme): string
    {
        $proxy = getenv(self::SCHEME_ENVIRONMENT_VAR_MAP[$scheme]);
        if (false === $proxy) {
            $proxy = '';
        }

        return $proxy;
    }

    /**
     * @param resource $context
     *
     * @return resource
     */
    private static function addProxyToStreamContext(string $proxy, $context = null)
    {
        $proxy = self::removeProtocol($proxy);
        if (null === $context) {
            $context = stream_context_create();
        }
        // the http is also required for https
        // https://www.php.net/manual/en/context.http.php#121762
        stream_context_set_option($context, [
            'http' => [
                'proxy' => "tcp://$proxy",
            ],
        ]);

        return $context;
    }

    private static function removeProtocol(string $url): string
    {
        return str_replace(self::PROTOCOLS, '', $url);
    }
}

/**
 * @param resource $context
 */
function file_get_contents_proxy(string $path, $context = null): string
{
    return ProxyTools::proxyAwareFileGetContents($path, $context);
}
